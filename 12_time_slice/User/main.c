
#include <rtdef.h>
#include <rtthread.h>
#include <rthw.h>
#include "ARMCM3.h"

int flag1;
int flag2;
int flag3;
int tick_flag;

struct rt_thread rt_flag1_thread;
struct rt_thread rt_flag2_thread;
struct rt_thread rt_flag3_thread;

rt_uint8_t rt_flag1_thread_stack[512];
rt_uint8_t rt_flag2_thread_stack[512];
rt_uint8_t rt_flag3_thread_stack[512];

extern rt_list_t rt_thread_priority_table[RT_THREAD_PRIORITY_MAX];


void delay(int t){
    for(; t != 0; t--);
}

void flag1_thread_entry(void *p_arg){
    for(; ; ) {
        flag1 = 1;
        rt_thread_delay(6);
        flag1 = 0;
        rt_thread_delay(6);
    }
}

void flag2_thread_entry(void *p_arg){
    for(; ; ) {
        flag2 = 1;
        // rt_thread_delay(3);
        delay(100);
        flag2 = 0;
        // rt_thread_delay(3);
        delay(100);
    }
}

void flag3_thread_entry(void *p_arg){
    for(; ; ) {
        flag3 = 1;
        // rt_thread_delay(3);
        delay(100);
        flag3 = 0;
        // rt_thread_delay(3);
        delay(100);
    }
}

int main(){
    
    /* 硬件相关初始化 */
    
    /* 关中断 */
    rt_hw_interrupt_disable();
    
    /* 设置SysTick中断频率 25MHz / 100 */
    SysTick_Config(SystemCoreClock / RT_TICK_PER_SECOND);
    
    /* 系统定时器列表初始化 */
    rt_system_timer_init();
    
    /* 初始化系统调度器 */
    rt_system_scheduler_init();
    
    /* 初始化空闲线程 */    
    rt_thread_idle_init();
    
    /* 初始化线程 */
    rt_thread_init( &rt_flag1_thread,                   /* 线程控制块 */
                    "rt_flag1_thread",                  /* 线程名字，字符串形式 */
                    flag1_thread_entry,                 /* 线程入口地址 */    
                    RT_NULL,                            /* 线程形参 */
                    &rt_flag1_thread_stack[0],          /* 线程栈起始地址 */    
                    sizeof(rt_flag1_thread_stack),      /* 线程栈大小 */
                    2,                                  /* 线程优先级 */
                    4);                                 /* 时间片 */
    
//    rt_list_insert_before(&(rt_thread_priority_table[0]), &(rt_flag1_thread.tlist));
    rt_thread_startup(&rt_flag1_thread);    // 启动线程，但是并不会被立即执行
                    
    rt_thread_init( &rt_flag2_thread,
                    "rt_flag2_thread",                /* 线程名字，字符串形式 */
                    flag2_thread_entry,
                    RT_NULL,
                    &rt_flag2_thread_stack[0],
                    sizeof(rt_flag2_thread_stack),
                    3,
                    2);
    
//    rt_list_insert_before(&(rt_thread_priority_table[1]), &(rt_flag2_thread.tlist));
    rt_thread_startup(&rt_flag2_thread);
    
    rt_thread_init( &rt_flag3_thread,
                    "rt_flag3_thread",                /* 线程名字，字符串形式 */
                    flag3_thread_entry,
                    RT_NULL,
                    &rt_flag3_thread_stack[0],
                    sizeof(rt_flag3_thread_stack),
                    3,
                    4);
    
//    rt_list_insert_before(&(rt_thread_priority_table[2]), &(rt_flag3_thread.tlist));
    rt_thread_startup(&rt_flag3_thread);
    
    rt_system_scheduler_start();
    
}


void SysTick_Handler(void)
{
    /* 进入中断 */
    rt_interrupt_enter();
    
    tick_flag = !tick_flag;

    rt_tick_increase();

    /* 离开中断 */
    rt_interrupt_leave();
}
