
#include <rtdef.h>
#include <rtthread.h>

int flag1;
int flag2;
int flag3;

struct rt_thread rt_flag1_thread;
struct rt_thread rt_flag2_thread;
struct rt_thread rt_flag3_thread;

rt_uint8_t rt_flag1_thread_stack[512];
rt_uint8_t rt_flag2_thread_stack[512];
rt_uint8_t rt_flag3_thread_stack[512];

extern rt_list_t rt_thread_priority_table[RT_THREAD_PRIORITY_MAX];


void delay(int t){
    for(; t != 0; t--);
}

void flag1_thread_entry(void *p_arg){
    for(; ; ) {
        flag1 = 1;
        delay(100);
        flag1 = 0;
        delay(100);
        rt_schedule_3();
    }
}

void flag2_thread_entry(void *p_arg){
    for(; ; ) {
        flag2 = 1;
        delay(100);
        flag2 = 0;
        delay(100);
        rt_schedule_3();
    }
}

void flag3_thread_entry(void *p_arg){
    for(; ; ) {
        flag3 = 1;
        delay(100);
        flag3 = 0;
        delay(100);
        rt_schedule_3();
    }
}

int main(){
    
    /* 初始化系统调度器 */
    rt_system_scheduler_init();
    
    /* 初始化线程 */
    rt_thread_init( &rt_flag1_thread,
                    "rt_flag1_thread",                /* 线程名字，字符串形式 */
                    flag1_thread_entry,
                    RT_NULL,
                    &rt_flag1_thread_stack[0],
                    sizeof(rt_flag1_thread_stack) );
    
    rt_list_insert_before(&(rt_thread_priority_table[0]), &(rt_flag1_thread.tlist));
    
    rt_thread_init( &rt_flag2_thread,
                    "rt_flag2_thread",                /* 线程名字，字符串形式 */
                    flag2_thread_entry,
                    RT_NULL,
                    &rt_flag2_thread_stack[0],
                    sizeof(rt_flag2_thread_stack) );
    
    rt_list_insert_before(&(rt_thread_priority_table[1]), &(rt_flag2_thread.tlist));
    
    rt_thread_init( &rt_flag3_thread,
                    "rt_flag3_thread",                /* 线程名字，字符串形式 */
                    flag3_thread_entry,
                    RT_NULL,
                    &rt_flag3_thread_stack[0],
                    sizeof(rt_flag3_thread_stack) );
    
    rt_list_insert_before(&(rt_thread_priority_table[2]), &(rt_flag3_thread.tlist));
    
    rt_system_scheduler_start();
    
}

